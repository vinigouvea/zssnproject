class ReportsController < ApplicationController

	# GET /reports/infected_count
	def infected_count
		@num_infected = Person.where(:infected => true).count(:id)
		@num_people = Person.count(:id)
		infecteds  = (@num_infected.to_f/@num_people)
		output = {'num_infected' => infecteds}.to_json
		render json: output 
	end

	# GET /reports/non_infected_count
	def non_infected_count
		@num_non_infected = Person.where(:infected => false).count(:id)
		@num_people = Person.count(:id)
		non_infecteds  = (@num_non_infected.to_f/@num_people)
		output = {'num_non_infected' => non_infecteds}.to_json
		render json: output 
	end

	# GET /reports/non_infected_count
	def people_inventory
		
		
	end



end