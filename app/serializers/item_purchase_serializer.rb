class ItemPurchaseSerializer < ActiveModel::Serializer
  attributes :id, :type, :count
  has_one :people
end
