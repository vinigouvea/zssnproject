class PersonSerializer < ActiveModel::Serializer
  attributes :id, :name, :age, :gender, :lonlat, :infected
  has_many :item_purchase
  link(:self) { person_url(object) }
end
