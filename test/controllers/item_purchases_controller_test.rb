require 'test_helper'

class ItemPurchasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item_purchase = item_purchases(:one)
  end

  test "should get index" do
    get item_purchases_url, as: :json
    assert_response :success
  end

  test "should create item_purchase" do
    assert_difference('ItemPurchase.count') do
      post item_purchases_url, params: { item_purchase: { count: @item_purchase.count, people_id: @item_purchase.people_id, type: @item_purchase.type } }, as: :json
    end

    assert_response 201
  end

  test "should show item_purchase" do
    get item_purchase_url(@item_purchase), as: :json
    assert_response :success
  end

  test "should update item_purchase" do
    patch item_purchase_url(@item_purchase), params: { item_purchase: { count: @item_purchase.count, people_id: @item_purchase.people_id, type: @item_purchase.type } }, as: :json
    assert_response 200
  end

  test "should destroy item_purchase" do
    assert_difference('ItemPurchase.count', -1) do
      delete item_purchase_url(@item_purchase), as: :json
    end

    assert_response 204
  end
end
