class CreateItemPurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :item_purchases do |t|
      t.references :people, foreign_key: true
      t.string :type
      t.integer :count

      t.timestamps
    end
  end
end
