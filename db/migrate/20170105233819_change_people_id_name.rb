class ChangePeopleIdName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :item_purchases, :people_id, :person_id
  end
end
