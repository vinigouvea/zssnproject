class ChangeTypeName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :item_purchases, :type, :itemCategory
  end
end
