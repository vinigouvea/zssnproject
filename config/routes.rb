Rails.application.routes.draw do

  resources :item_purchases
  resources :people

  #this route for the reports infected person 
  get 'reports/infected_count', to: "reports#infected_count"

  #this route for the reports infected person 
  get 'reports/non_infected_count', to: "reports#non_infected_count"
 
end